import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from './message'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()
  const login = async function (email: string, password: string) {
    loadingStore.doLoad()
    try {
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      messageStore.showMessage('Login Success')
      router.push('/')
    } catch (e: any) {
      console.log(e)
      messageStore.showMessage(e.message)
    }
    loadingStore.finish()
  }
  const logout = function () {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    // ถ้าใช้ replace มันจะback ไม่ได้ นะจ๊ะ
    // ถ้าใช้ Push จะback ได้
    router.replace('/login')
  }

  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }

  function getToken(): string | null {
    const srtToken = localStorage.getItem('access_token')
    if (srtToken === null) return null
    return (srtToken)
  }
  return { getCurrentUser, login, getToken, logout }
})
