import router from '@/router'
import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

function delay(sec: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(sec), sec * 1000)
  })
}
// วิธี่ดึงเอา token ออก มากใช้ login
// instance.interceptors.request.use ใช้ ต่อเมื่อ เราrequest ไปแล้วก็ให้ทำอย่างนี้
instance.interceptors.request.use(
  async function (config) {
    const token = localStorage.getItem('access_token')
    console.log('request interceptor ' + token)

    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    console.log(config)
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)
// this is when after request it will respond
instance.interceptors.response.use(
  async function (res) {
    console.log('response interceptor')

    await delay(1)
    return res
  },
  function (error) {
    if (401 === error.response.status) {
      router.replace('login')
    }
    return Promise.reject(error)
  }
)
export default instance
